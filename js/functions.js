$(document).ready(function() {

    // $(this).scrollTop(0);

    $('.carousel').slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        speed: 1000,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
            }
        }, {
            breakpoint: 601,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });

    $('.testimonials-slide').slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
    });

    $(function() {
        var url = "url of the modal window content";
        $('.my-modal-cont').load(url, function(result) {
            $('#myModal').modal({ show: true });
        });
    });

    // Ease scroll 
    $('.header a[href^="#"], .footer-top a[href^="#"]').on('click', function(e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function() {
            // window.location.hash = target;
        });

        $('.navbar-toggle').click(); //bootstrap 3.x by Richard

        return false;
    });


$('.modal-product').on('hide.bs.modal', function(){
    $('.modal-body').fadeOut();
});

$( ".product-carousel .item a" ).each(function(index) {
    $(this).on("click", function(){
    var productName = $(this).attr("data-product");
    $('.modal-product').find('.' + productName).show().siblings('.modal-body').hide();
    });
});


});
